package com.example.tp1;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        Intent intent = getIntent();
        final String countryName = intent.getStringExtra("countryName");
        final Country country = CountryList.getCountry(countryName);
        final String image = country.getmImgFile();
        final Resources res = this.getResources();
        int resId = res.getIdentifier(image, "drawable", getPackageName());

        ImageView flagImage = findViewById(R.id.flagView);
        flagImage.setImageResource(resId);

        TextView titleView = findViewById(R.id.titleView);
        titleView.setText(countryName);

        final EditText capitalInput = findViewById(R.id.capitalInput);
        capitalInput.setText(country.getmCapital());

        final EditText languageInput = findViewById(R.id.languageInput);
        languageInput.setText(country.getmLanguage());

        final EditText currencyInput = findViewById(R.id.currencyInput);
        currencyInput.setText(country.getmCurrency());

        final EditText populationInput = findViewById(R.id.populationInput);
        populationInput.setText(String.format(Locale.FRANCE, "%d", country.getmPopulation()));

        final EditText areaInput = findViewById(R.id.areaInput);
        areaInput.setText(String.format(Locale.FRANCE, "%d", country.getmArea()));
        Button saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmCapital(capitalInput.getText().toString());
                country.setmLanguage(languageInput.getText().toString());
                country.setmCurrency(currencyInput.getText().toString());
                country.setmPopulation(Integer.parseInt(populationInput.getText().toString()));
                country.setmArea(Integer.parseInt(areaInput.getText().toString()));
                finish();
            }
        });
    }
}
