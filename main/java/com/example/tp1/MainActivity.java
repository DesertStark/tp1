package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView countryListView = findViewById(R.id.countryList);
        String[] values = CountryList.getNameArray();
        final ArrayAdapter<String> countryListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        countryListView.setAdapter(countryListAdapter);
        countryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, CountryActivity.class);
                intent.putExtra("countryName", (String) parent.getItemAtPosition(position));
                startActivity(intent);
            }
        });
    }
}

